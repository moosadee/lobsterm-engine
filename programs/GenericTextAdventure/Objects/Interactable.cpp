#include "Interactable.h"

#include "../../../engine/Log.h"
#include "../../../engine/Helper.h"

Interactable::Interactable()
{
  m_type = "Interactable";
}

Interactable::Interactable( std::string newName, std::string newDescription, std::string newLocation )
{
  m_type = "Interactable";
  Setup( newName, newDescription, newLocation );
}

void Interactable::Setup( std::string newName, std::string newDescription, std::string newLocation )
{
  m_name = newName;
  m_description = newDescription;
  m_location = newLocation;
}

void Interactable::AddKeywordAction( std::vector<std::string> keywords, std::vector< Action > actions )
{
  std::string loglist = "";
  for ( auto& a : actions )
  {
    loglist += "[" + a.type + "|" + a.content + "] ";
  }
  Log::Out( "For keywords [" + Helper::VectorToString( keywords ) + "] Add actions: " + loglist, "Interactable::AddKeywordAction" );
  for ( auto& word : keywords )
  {
    m_keywordActions[ word ] = actions;
  }
}

std::vector< Action > Interactable::GetKeywordAction( std::string keyword )
{
  if ( m_keywordActions.find( keyword ) != m_keywordActions.end() )
  {
    return m_keywordActions.at( keyword );
  }
  return {};
}

std::map<std::string, std::vector< Action > > Interactable::GetKeywordActions()
{
  return m_keywordActions;
}

void Interactable::SetLocation( std::string locationName )
{
  m_location = locationName;
}

std::string Interactable::GetLocation() const
{
  return m_location;
}

void Interactable::AddImage( std::string name, std::string imagePath )
{
    m_images[name].Open( imagePath );
}

PpmImage Interactable::GetImage( std::string name ) const
{
    return m_images.at( name );
}

std::string Interactable::GetName() const
{
  return m_name;
}

std::string Interactable::GetDescription() const
{
  return m_description;
}

std::string Interactable::GetType() const
{
  return m_type;
}



