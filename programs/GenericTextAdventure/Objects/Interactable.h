#ifndef _INTERACTABLE
#define _INTERACTABLE

#include <map>
#include <vector>
#include <string>

#include "../../../engine/PpmImage.h"

struct Action
{
  Action() { }
  Action( std::string newType, std::string newContent )
  {
    type = newType;
    content = newContent;
  }
  std::string type;
  std::string content;
};

class Interactable
{
public:
  Interactable();
  Interactable( std::string newName, std::string newDescription, std::string newLocation );
  void Setup( std::string newName, std::string newDescription, std::string newLocation );

  void AddKeywordAction( std::vector<std::string> keywords, std::vector<Action> actions );
  std::vector<Action> GetKeywordAction( std::string keyword );
  std::map<std::string, std::vector<Action> > GetKeywordActions();

  void SetLocation( std::string locationName );
  std::string GetLocation() const;

  void AddImage( std::string name, std::string imagePath );
  PpmImage GetImage( std::string name ) const;

  std::string GetName() const;
  std::string GetDescription() const;
  std::string GetType() const;

protected:
  std::string m_location;
  std::string m_name;
  std::string m_description;
  std::string m_type;
  std::map<std::string, PpmImage> m_images;
  std::map<std::string, std::vector<Action> > m_keywordActions;
};

#endif
