#include "Enums.h"

Direction GetOpposite( Direction dir )
{
  switch( dir )
  {
    case Direction::NORTH:      return Direction::SOUTH; break;
    case Direction::SOUTH:      return Direction::NORTH; break;
    case Direction::EAST:       return Direction::WEST; break;
    case Direction::WEST:       return Direction::EAST; break;
    case Direction::OUT:        return Direction::IN; break;
    case Direction::IN:         return Direction::OUT; break;
    case Direction::DOWN:       return Direction::UP; break;
    case Direction::UP:         return Direction::DOWN; break;
    case Direction::LEFT:       return Direction::RIGHT; break;
    case Direction::RIGHT:      return Direction::LEFT; break;
    case Direction::FORWARD:    return Direction::BACKWARD; break;
    case Direction::BACKWARD:   return Direction::FORWARD; break;
    default:                    return Direction::UNKNOWN;
  }
}

Direction FirstDirection()
{
  return Direction::UNKNOWN;
}

Direction LastDirection()
{
  return Direction::RIGHT;
}

std::string DirectionStr( Direction dir )
{
  switch( dir )
  {
    case Direction::NORTH:      return "SOUTH";     break;
    case Direction::SOUTH:      return "NORTH";     break;
    case Direction::EAST:       return "WEST";      break;
    case Direction::WEST:       return "EAST";      break;
    case Direction::OUT:        return "OUT";       break;
    case Direction::IN:         return "IN";        break;
    case Direction::UP:         return "UP";        break;
    case Direction::DOWN:       return "DOWN";      break;
    case Direction::FORWARD:    return "FORWARD";   break;
    case Direction::BACKWARD:   return "BACKWARD";  break;
    case Direction::LEFT:       return "LEFT";  break;
    case Direction::RIGHT:      return "RIGHT";  break;
    default:                    return "UNKNOWN";
  }
}
