#ifndef _PROGRAM
#define _PROGRAM

#include "Map/Location.h"
#include "Objects/Interactable.h"
#include <map>
#include <vector>
#include <string>

class Program
{
public:
  Program();
  ~Program();

  void Setup();
  void Teardown();

  void Main();

private:
  std::map<std::string, Location> m_locations;
  std::vector<Interactable*> m_objects;

  std::string m_currentLocKey;
  std::vector<std::string> m_status;
  std::vector<std::string> m_inventory;
  std::string m_currentTab;

  Location& GetCurrentLocation();
  Location& GetLocation( std::string key );

  void DrawScreen();
  void DrawView( int screen_width, int screen_height, int top_y );
  void DrawDescription( int screen_width, int screen_height, int top_y );
  void DrawMenu( int screen_width, int screen_height, int top_y );

  void SetNeighbors( std::string key1, Direction isBlankOf, std::string key2 );

  void HandleInput( std::string choice );
  bool CheckMovement( std::string choice );
  bool CheckUIChange( std::string choice );
  bool CheckInteraction( std::string choice );
};

#endif
