#include "Program.h"
#include "../../engine/ScreenDrawer.h"
#include "../../engine/Helper.h"
#include "../../engine/Log.h"

#include <string>
#include <iostream>

Program::Program()
{
  Log::Setup();
  Setup();
}

Program::~Program()
{
  Teardown();
  Log::Teardown();
}

void Program::Setup()
{
//  Log::Out( "Function begins", "Program::Setup" );
  ScreenDrawer::Setup( 42, 31 );
  ScreenDrawer::SetBackgroundColors( "white", "blue", ' ' );

  m_locations["DoomA"]   = Location( "Assets/Images/doomA.ppm", "ASDF", {} );
  m_locations["DoomB"]   = Location( "Assets/Images/doomB.ppm", "", {} );
  m_locations["DoomC"]   = Location( "Assets/Images/doomC.ppm", "", {} );
  m_locations["DoomD"]   = Location( "Assets/Images/doomD.ppm", "", {} );
  m_locations["DoomE"]   = Location( "Assets/Images/doomE.ppm", "", {} );
  m_locations["DoomF"]   = Location( "Assets/Images/doomF.ppm", "", {} );
  m_locations["DoomG"]   = Location( "Assets/Images/doomG.ppm", "", {} );
  m_locations["DoomH"]   = Location( "Assets/Images/doomH.ppm", "", {} );
  m_locations["DoomI"]   = Location( "Assets/Images/doomI.ppm", "", {} );
  m_locations["DoomJ"]   = Location( "Assets/Images/doomJ.ppm", "", {} );
  m_locations["NOWHERE"] = Location( "Assets/Images/desk.ppm", "asdfasdf", {} );

  m_currentLocKey = "DoomA";

  SetNeighbors( "DoomA",   Direction::FORWARD,  "DoomD" );
  SetNeighbors( "DoomA",   Direction::LEFT,     "DoomB" );

  SetNeighbors( "DoomB",   Direction::FORWARD,  "DoomC" );

  SetNeighbors( "DoomD",   Direction::FORWARD,  "DoomE" );

  SetNeighbors( "DoomE",   Direction::FORWARD,  "DoomF" );
  SetNeighbors( "DoomE",   Direction::RIGHT,    "DoomG" );

  SetNeighbors( "DoomG",   Direction::FORWARD,  "DoomH" );

  SetNeighbors( "DoomH",   Direction::FORWARD,  "DoomI" );

  SetNeighbors( "DoomI",   Direction::RIGHT,    "DoomJ" );

  Interactable* armor = new Interactable( "Armor", "", "DoomC" );

  armor->AddImage( "default", "Assets/Images/armor.ppm" );
  armor->AddKeywordAction( { "look" },
    { Action( "STATUS", "Green armor absorbs 1/3rd of damage" ) } );

  armor->AddKeywordAction( { "take", "get", "pickup" },
    { Action( "STATUS", "You take the armor." ),
    Action( "INVENTORYADD", "Green armor (100%)" ),
    Action( "OBJECTREMOVE", "" ) } );

  Log::Out( "armor is at", armor->GetLocation(), "Program::Setup" );

  Interactable* baddie = new Interactable( "Zombie", "", "DoomF" );
  baddie->AddImage( "default", "Assets/Images/doombad.ppm" );
  baddie->AddKeywordAction( { "talk" },
    { Action( "STATUS", "You say \"Would you kindly move?\"" ),
    Action( "STATUS", "He responds \"Yeah, sure.\"" ),
    Action( "OBJECTREMOVE", "" ) }
    );

  Log::Out( "baddie is at", baddie->GetLocation(), "Program::Setup" );

  Interactable* imp = new Interactable( "Imp", "", "DoomH" );
  imp->AddImage( "default", "Assets/Images/doomimp.ppm" );
  imp->AddKeywordAction( { "talk" },
    { Action( "STATUS", "You say \"I'm in a hurry, sorry.\"" ),
    Action( "STATUS", "He responds \"My bad.\" and leaves." ),
    Action( "OBJECTREMOVE", "" ) }
    );
  Log::Out( "imp is at", imp->GetLocation(), "Program::Setup" );

  Interactable* exitswitch = new Interactable( "Exit Switch", "", "DoomJ" );
  exitswitch->AddImage( "default", "doomexit.ppm" );
  exitswitch->AddKeywordAction( { "flip" },
    { Action( "STATUS", "You flip the switch." ),
    Action( "STATUS", "You won the game!" ) }
    );
  Log::Out( "exitswitch is at", exitswitch->GetLocation(), "Program::Setup" );

  m_objects.push_back( armor );
  m_objects.push_back( baddie );
  m_objects.push_back( imp );
  m_objects.push_back( exitswitch );

  m_status.clear();
  m_currentTab = "inventory";
  m_inventory.push_back( "Health (100%)" );
  m_inventory.push_back( "Pistol" );
  m_inventory.push_back( "Fist" );
}

void Program::SetNeighbors( std::string this_place, Direction isBlankOf, std::string that_place )
{
  Log::Out( "Neighbors: " + this_place + " <-> " + that_place, "Program::SetNeighbors" );
  GetLocation( this_place ).SetNeighbor( isBlankOf, that_place );
  GetLocation( that_place ).SetNeighbor( GetOpposite( isBlankOf ), this_place );
}

void Program::Teardown()
{
  ScreenDrawer::Teardown();

  for ( auto* ptr : m_objects )
  {
    if ( ptr != nullptr )
    {
      delete ptr;
    }
  }
}

void Program::Main()
{
//  Log::Out( "Function begins", "Program::Main" );
  bool done = false;
  while ( !done )
  {
    DrawScreen();
    ScreenDrawer::Draw();

    std::string choice;
    std::cout << "What do you want to do? ";
    getline( std::cin, choice );
    choice = Helper::ToLower( choice );

    Log::Out( "User entered", choice, "Program::Main" );

    m_status.clear();

    if ( choice == "exit" )
    {
      done = true;
      continue;
    }

    HandleInput( choice );
  }
}

Location& Program::GetCurrentLocation()
{
    return GetLocation( m_currentLocKey );
}

Location& Program::GetLocation( std::string key )
{
    try
    {
        return m_locations[key];
    }
    catch( ... )
    {
        Log::Err( "COULDN'T FIND LOCATION WITH KEY \"" + key + "\"", "Program::GetCurrentLocation" );
    }
}

void Program::DrawScreen()
{
//  Log::Out( "Function begins", "Program::DrawScreen" );
  ScreenDrawer::Fill( ' ', "black", "black" );

  size_t scrw = ScreenDrawer::GetScreenWidth();
  size_t scrh = ScreenDrawer::GetScreenHeight();

  // Draw frame around game view
  //ScreenDrawer::DrawRect( 0, 0, scrw-1, scrh-1, '/', "blue", "black" );

  DrawMenu( scrw, scrh, 3 ); // Height: 10
  DrawView( scrw, scrh, 10 ); // Height: 10
  DrawDescription( scrw, scrh, 20 );
}

void Program::DrawView( int screen_width, int screen_height, int top_y )
{
//  Log::Out( "Function begins", "Program::DrawView" );
  ScreenDrawer::DrawFillRect( 0, top_y, screen_width, top_y+screen_height, ' ', "blue", "blue" );

  //ScreenDrawer::DrawRect( 0, top_y, screen_width-1, top_y+11, ' ', "black", "black" );
  std::vector<PpmImage> layers;
  layers.push_back( GetCurrentLocation().GetImage() );

  // What objects are on this map?
  for ( auto* obj : m_objects )
    {
      if ( obj->GetLocation() == m_currentLocKey )
      {
          layers.push_back( obj->GetImage( "default" ) );
      }
    }

  ScreenDrawer::DrawLayered( layers, 1, top_y+1 );
}

void Program::DrawMenu( int screen_width, int screen_height, int top_y )
{
//  Log::Out( "Function begins", "Program::DrawMenu" );
  int bottom = top_y+5;
  //Draw tabbed region
  ScreenDrawer::DrawFillRect( 0, top_y, screen_width, top_y+screen_height, ' ', "blue", "blue" );

  ScreenDrawer::DrawRect( 0, top_y, screen_width-1, bottom, ' ', "white", "white" );

  if ( m_currentTab == "inventory" )
  {
    int x = 1, y = top_y+1;
    for ( size_t i = 0; i < m_inventory.size(); i++ )
    {
      ScreenDrawer::Set( x, y, "* " + m_inventory[i], "white", "black" );
      y++;
      if ( y == bottom )
      {
        y = top_y+1;
        x += 20;
      }
    }
  }
  else if ( m_currentTab == "stats" )
  {
  }
  else if ( m_currentTab == "minimap" )
  {
  }

  // tabs
  ScreenDrawer::DrawLine( 1, top_y, 10, top_y, ' ', "white", ( m_currentTab == "inventory" ) ? "white" : "blue" );
  ScreenDrawer::Set( 1, top_y, "INVENTORY", "black", ( m_currentTab == "inventory" ) ? "white" : "blue" );

//  ScreenDrawer::DrawLine( 13, top_y, 23, top_y, ' ', "blue", (m_currentTab == "stats") ? "white" : "blue" );
//  ScreenDrawer::Set( 13, top_y, "STATS", "black", (m_currentTab == "stats") ? "white" : "blue" );

  ScreenDrawer::DrawLine( 25, top_y, 35, top_y, ' ', "blue", ( m_currentTab == "minimap" ) ? "white" : "blue" );
  ScreenDrawer::Set( 25, top_y, "MINIMAP", "black", ( m_currentTab == "minimap" ) ? "white" : "blue" );
}

void Program::DrawDescription( int screen_width, int screen_height, int top_y )
{
//  Log::Out( "Function begins", "Program::DrawDescription" );
  // bottom region
  Log::Out( "Current location", GetCurrentLocation().GetName(), "Program::DrawScreen" );

  ScreenDrawer::DrawFillRect( 0, top_y, screen_width, top_y+screen_height, ' ', "blue", "blue" );

  int y = top_y+0;
  ScreenDrawer::Set( 1, 0, "LOCATION: " + GetCurrentLocation().GetName(), "yellow", "black" );

//  y++;
  for ( auto& d : GetCurrentLocation().GetDescription() )
  {
    ScreenDrawer::Set( 1, y, d, "yellow", "black" );
    y++;
  }

  y++;

  ScreenDrawer::Set( 1, y, "You see... ", "yellow", "black" );
  y++;

  // What objects are on this map?
  int x = 2;
  bool seeSomething = false;
  for ( size_t i = 0; i < m_objects.size(); i++ )
  {
    if ( m_objects[i]->GetLocation() == m_currentLocKey )
    {
      ScreenDrawer::Set( x, y, "* " + m_objects[i]->GetName(), "yellow", "black" );
      seeSomething = true;
      x += 15;
      if ( x >= screen_width-15 )
      {
        x = 2;
        y++;
      }
    }
  }
  if ( seeSomething == false )
  {
    ScreenDrawer::Set( x, y, "Nothing of note", "yellow", "black" );
  }

  y += 2;

  ScreenDrawer::Set( 1, y, "You can go...", "yellow", "black" );

  x = 2; y++;
  for ( int i = static_cast<int>( FirstDirection() ); i <= static_cast<int>( LastDirection() ); i++ )
  {
    Direction dir = static_cast<Direction>( i );
    if ( GetCurrentLocation().HasNeighbor( dir ) )
    {
      ScreenDrawer::Set( x, y, "* " + DirectionStr( dir ), "yellow", "black" );
      x += 15;
      if ( x >= screen_width-15 )
      {
        x = 2;
        y++;
      }
    }
  }
  y += 2;

  std::string s = "";
  for ( auto& stat : m_status ) { s += stat + "\n"; }
  Log::Out( "Game response", s, "Program::HandleInput" );

  ScreenDrawer::Set( 1, y, "ACTION:", "cyan", "black" );
  for ( auto& s : m_status )
  {
    ScreenDrawer::Set( 1, ++y, s, "cyan", "black" );
  }
}

void Program::HandleInput( std::string choice )
{
//  Log::Out( "Function begins", "Program::HandleInput" );

  if      ( CheckMovement( choice ) ) { }
  else if ( CheckInteraction( choice ) ) { }
  else if ( CheckUIChange( choice ) ) { }
}

bool Program::CheckMovement( std::string choice )
{
//  Log::Out( "Function begins", "Program::CheckMovement" );
  // Did they type a direction?
  bool moved = false;
  for ( int i = static_cast<int>( FirstDirection() ); i <= static_cast<int>( LastDirection() ); i++ )
  {
    Direction dir = static_cast<Direction>( i );
    std::string dirStr = Helper::ToLower( DirectionStr( dir ) );
    std::string firstLetter = "";
    firstLetter += dirStr[0];

    if ( ( choice == dirStr || choice == firstLetter ) && GetCurrentLocation().HasNeighbor( dir ) )
    {
      m_currentLocKey = GetCurrentLocation().GetNeighborKey( dir );
      m_status.push_back( "You went " + dirStr + "." );
      moved = true;
    }
    else if ( ( choice == dirStr || choice == firstLetter ) && !GetCurrentLocation().HasNeighbor( dir ) )
    {
      m_status.push_back( "You cannot move in that direction!" );
    }
  }

  return moved;
}

bool Program::CheckUIChange( std::string choice )
{
  if ( choice == "inventory" )
  {
    m_currentTab = "inventory";
    return true;
  }
  else if ( choice == "minimap" )
  {
    m_currentTab = "minimap";
    return true;
  }
  else if ( choice == "stats" )
  {
    m_currentTab = "stats";
    return true;
  }

  return false;
}

bool Program::CheckInteraction( std::string choice )
{
//  Log::Out( "Function begins", "Program::CheckInteraction" );
//  armor->AddKeywordAction( { "look" }, { Action( "STATUS", "Green armor absorbs 1/3rd of damage" ) } );
//  armor->AddKeywordAction( { "take", "get", "pickup" }, { Action( "INVENTORYADD", "Green armor (100%)" ) } );

  std::string objRef = "";
  size_t objIndex = 0;
  for ( size_t i = 0; i < m_objects.size(); i++ )
  {
    // Is this object on the map?
    if ( m_objects[i]->GetLocation() != m_currentLocKey )
    {
      continue;
    }

    // Is this object's name referenced in the choice text?
    if ( Helper::Contains( choice, m_objects[i]->GetName(), false ) )
    {
      objRef = m_objects[i]->GetName();
      objIndex = i;
      break;
    }
  }

  if ( objRef == "" ) { return false; }
  Log::Out( "Object referenced", objRef, "Program::CheckInteraction" );

  std::map<std::string, std::vector<Action> > objActions = m_objects[objIndex]->GetKeywordActions();


  std::string keyRef = "";
  for ( auto& kv : objActions )
  {
    // Was this action keyword mentioned in the choice text?
    if ( Helper::Contains( choice, kv.first, false ) )
    {
      keyRef = kv.first;
      break;
    }
  }

  if ( keyRef == "" ) { return false; }
  Log::Out( "Action referenced", keyRef, "Program::CheckInteraction" );

  for ( auto& action : objActions[ keyRef ] )
  {
    if ( action.type == "STATUS" )
    {
      m_status.push_back( action.content );
    }
    else if ( action.type == "INVENTORYADD" )
    {
      m_inventory.push_back( action.content );
    }
    else if ( action.type == "OBJECTREMOVE" )
    {
      m_objects[objIndex]->SetLocation( "NOWHERE" );
    }
  }

  // Check for interaction with objects
  return true;
}




