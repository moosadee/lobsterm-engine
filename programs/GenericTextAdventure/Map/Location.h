#ifndef _LOCATION
#define _LOCATION

#include "../../../engine/PpmImage.h"
#include "../Enums.h"
#include <string>
#include <vector>
#include <map>

class Location
{
  public:
  Location();
  Location( std::string imagePath, std::string newName, std::vector<std::string> newDescription );
  void InitializeNeighbors();
  void Setup( std::string imagePath, std::string newName, std::vector<std::string> newDescription );
  void SetNeighbor( Direction direction, std::string lockey );
  std::string GetNeighborKey( Direction direction );
  bool HasNeighbor( Direction direction );

  PpmImage& GetImage();
  std::string GetName();
  std::vector<std::string> GetDescription();

  private:
  std::string m_name;
  std::vector<std::string> m_description;
  PpmImage m_image;
  std::map<Direction, std::string> m_neighbors;
};

#endif
