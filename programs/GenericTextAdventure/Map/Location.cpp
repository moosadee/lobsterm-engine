#include "Location.h"
#include "../../../engine/Log.h"

Location::Location()
{
//  Log::Out( "Function begin", "Location::Location" );
  m_name = "UNSET";
  InitializeNeighbors();
}

Location::Location( std::string imagePath, std::string newName, std::vector<std::string> newDescription )
{
//  Log::Out( "Function begin", "Location::Location 2" );
  Setup( imagePath, newName, newDescription );
  InitializeNeighbors();
}

void Location::Setup( std::string imagePath, std::string newName, std::vector<std::string> newDescription )
{
//  Log::Out( "Function begin", "Location::Setup" );
  m_name = newName;
  m_description = newDescription;
  m_image.Open( imagePath );
}

void Location::InitializeNeighbors()
{
//  Log::Out( "Function begin", "Location::InitializeNeighbors" );
  for ( int i = static_cast<int>( FirstDirection() ); i <= static_cast<int>( LastDirection() ); i++ )
  {
    m_neighbors[ static_cast<Direction>( i ) ] = "";
  }
}

void Location::SetNeighbor( Direction direction, std::string lockey )
{
//  Log::Out( "Function begin", "Location::SetNeighbor" );
  m_neighbors[direction] = lockey;
}

PpmImage& Location::GetImage()
{
//  Log::Out( "Function begin", "Location::GetImage" );
  return m_image;
}

std::string Location::GetName()
{
//  Log::Out( "Function begin", "Location::GetName" );
  return m_name;
}

std::vector<std::string> Location::GetDescription()
{
//  Log::Out( "Function begin", "Location::GetDescription" );
  return m_description;
}

std::string Location::GetNeighborKey( Direction direction )
{
//  Log::Out( "Function begin", "Location::GetNeighborKey" );
    return m_neighbors[direction];
}

bool Location::HasNeighbor( Direction direction )
{
//  Log::Out( "Function begin", "Location::HasNeighbor" );
  return ( m_neighbors.find( direction ) != m_neighbors.end() && m_neighbors.at( direction ) != "" );
}




