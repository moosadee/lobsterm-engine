#ifndef _ENUM
#define _ENUM

#include <string>

enum class Direction {
    UNKNOWN = 0, NORTH = 1, SOUTH = 2, EAST = 3, WEST = 4,
    IN = 5, OUT = 6,
    UP = 7, DOWN = 8,
    FORWARD = 9, BACKWARD = 10,
    LEFT = 11, RIGHT = 12
};

Direction GetOpposite( Direction dir );
Direction FirstDirection();
Direction LastDirection();
std::string DirectionStr( Direction dir );

#endif
