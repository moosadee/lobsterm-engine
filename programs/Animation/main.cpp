#include "../../engine/ScreenDrawer.h"
#include "../../engine/PpmImage.h"
#include "../../engine/Helper.h"
#include "../../engine/Log.h"

#include <string>
#include <sstream>
#include <iostream>
#include <vector>
using namespace std;


void LoadFrames( vector<PpmImage>& frames, int frameCount )
{
    for ( int i = 1; i < frameCount; i++ )
    {
        string path = "ppm2/frame";
        if ( i < 1000 ) { path += "0"; }
        if ( i < 100 ) { path += "0"; }
        if ( i < 10 ) { path += "0"; }
        path += Helper::ToString( i );
        path += ".ppm";

//    Log::Out( "Load", path, "LoadFrames" );

        PpmImage new_frame( path );
        frames.push_back( new_frame );
    }
}

int main()
{
    Log::Setup();
    vector<PpmImage> frames;
    LoadFrames( frames, 155 );
    int fps = 4;

    cout << endl;
    cout << "Rachel Wil Sha Singh" << endl;
    Helper::Sleep( 1000 );
    int imageHeight = 20;

    ScreenDrawer::Setup( 80, imageHeight+1 );

    while ( true )
    {
        for ( size_t i = 0; i < frames.size(); i++ )
        {
            //    ScreenDrawer::Fill( ' ', "black", "black" );
            // Draw frame
            ScreenDrawer::DrawImage( frames[i], 0, 0 );

            ScreenDrawer::Set( 0, imageHeight, "Frame " + Helper::ToString( i ), "white", "black" );

            // Update screen
            ScreenDrawer::Draw();

            Helper::Sleep( 1000 / fps );
        }
    }


    ScreenDrawer::Teardown();
    Log::Teardown();
    return 0;
}
