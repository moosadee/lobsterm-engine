#include "Log.h"

std::ofstream Log::s_outfile;

void Log::Setup()
{
  s_outfile.open( "log.txt" );
  Out( "Logging begins!", "Log::Setup" );
}

void Log::Teardown()
{
  Out( "Logging ends!", "Log::Teardown" );
  s_outfile.close();
}

void Log::Out( std::string label, std::string text, std::string where /*= ""*/ )
{
  s_outfile << label << ": " << text << " (@ " << where << ")" << std::endl;
}

void Log::Out( std::string text, std::string where /*= ""*/ )
{
  s_outfile << text << " (@ " << where << ")" << std::endl;
}

void Log::Err( std::string text, std::string where /*= ""*/ )
{
  s_outfile << "!! " << text << " (@ " << where << ")" << std::endl;
}
