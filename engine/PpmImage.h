#ifndef _PPMIMAGE
#define _PPMIMAGE

#include <vector>
#include <string>

struct RGB
{
  RGB() { }
  RGB( int r, int g, int b )
  {
    this->r = r;
    this->g = g;
    this->b = b;
  }
  int r, g, b;
};

struct Pixel
{
  void SetRgb( int r, int g, int b )
  {
    this->r = r;
    this->g = g;
    this->b = b;
  }
  void SetXy( int x, int y )
  {
    this->x = x;
    this->y = y;
  }
  int r, g, b;
  int x, y;
  std::string foregroundColor;
  std::string backgroundColor;
};

class PpmImage
{
 public:
  PpmImage();
  PpmImage( std::string filename, int width, int height );
  PpmImage( std::string filename );

  void Create( std::string filename, int width, int height );
  void Open( std::string filename = "" );
  void Save( std::string filename = "" );

  int GetWidth() const;
  int GetHeight() const;
  void SetDimensions( int width, int height );
  void CreateCanvas( int width, int height );
  static std::string RgbToColorName( int r, int g, int b );
  static RGB ColorNameToRgb( std::string color );
  static std::string GetGoodTextColor( std::string backgroundColor );

  std::vector<Pixel>& GetPixels();
  void SetPixelColor( int x, int y, std::string ncursesColor );
  void FillAllPixels( std::string ncursesColor );

  std::string GetFilename() const;
  
 private:
  int m_width;
  int m_height;
  std::string m_filename;
  std::vector<Pixel> m_pixels;
};

#endif
