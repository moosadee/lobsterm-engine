#ifndef _SCREEN_DRAWER_HPP
#define _SCREEN_DRAWER_HPP

#include <string>
#include <map>
#include <vector>

#include "PpmImage.h"

//! Represents one tile (or "character") in the screen
struct ScreenTile {
  char symbol;
  std::string foreground;
  std::string background;
};

//! Class to control drawing colors and text to the screen
class ScreenDrawer
{
public:
  //! Initialize drawer with a screenWidth and screenHeight (recommend lower height than terminal window)
  static void Setup( int screenWidth, int screenHeight );
  //! Clean up the ScreenDrawer before closing program
  static void Teardown();
  //! Fill all the ScreenTiles with one set of symbols and some foreground/background colors.
  static void Fill( char symbol, std::string foreground, std::string background );
  static void Clear(int x, int y);
  //! Set a char value and foreground/background colors at some position on the screen.
  static void Set(int x, int y, char symbol, std::string foreground = "white", std::string background = "black");
  //! Set a string value and foreground/background colors at some position on the screen.
  static void Set(int x, int y, std::string str, std::string foreground = "white", std::string background = "black");
  //! Draw a filled-in rectangle shape between two points, filled with some character and with the given foreground/background colors.
  static void DrawFillRect( int left, int top, int right, int bottom, char symbol, std::string foreground = "white", std::string background = "black" );
  //! Draw a not-filled-in rectangle shape between two points, border will use symbol and given foreground/background colors.
  static void DrawRect( int left, int top, int right, int bottom, char symbol, std::string foreground = "white", std::string background = "black" );
  //! Draw a line between two points, using some character and foreground/background color.
  static void DrawLine( int x1, int y1, int x2, int y2, char symbol, std::string foreground = "white", std::string background = "black" );
  //! Display current ScreenTile array state to the screen
  static void Draw();
  //! Retrieve the set ScreenDrawer width
  static int GetScreenWidth();
  //! Retrieve the set ScreenDrawer height
  static int GetScreenHeight();

  static void SetCoutStyle(std::string bg, std::string fg);
  static void ResetCoutStyle();

  //! Set up window border foreground/background for all future calls to DrawWindow.
  static void SetBorderColors( std::string foreground, std::string background );
  //! Set up window foreground/background for all future calls to DrawWindow.
  static void SetWindowColors( std::string foreground, std::string background );
  //! Set up screen foreground/background and symbol for all future calls to DrawBackground.
  static void SetBackgroundColors( std::string foreground, std::string background, char texture );
  //! Set up "window title" region colors for all future calls to DrawWindow.
  static void SetWinTitleColors( std::string foreground, std::string background );

  //! Draw a filled in background on all ScreenTiles in the array.
  static void DrawBackground();
  //! Draw a "window" shape at the given position and size, with a "window title".
  static void DrawWindow( std::string title, int left, int top, int right, int bottom );

  //! Draw a PpmImage to the screen
  static void DrawImage( PpmImage image, int offsetX = 0, int offsetY = 0 );
  
  //! Draw multiple layers; layer 0 should be background. Invalid colors are "transparent".
  static void DrawLayered( std::vector<PpmImage> images, int offsetX = 0, int offsetY = 0 );

private:
  static size_t s_screenWidth;
  static size_t s_screenHeight;

  static bool IsValidRange(size_t x, size_t y);
  static void ClearConsole();

  static ScreenTile** s_grid;

  static std::map<std::string, int> s_backgrounds;
  static std::map<std::string, int> s_foregrounds;

  static ScreenTile s_borderColors;
  static ScreenTile s_windowColors;
  static ScreenTile s_backgroundColors;
  static ScreenTile s_winTitleColors;
};

#endif
