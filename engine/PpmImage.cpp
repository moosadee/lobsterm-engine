#include "PpmImage.h"
#include "ScreenDrawer.h"
#include "Log.h"

#include <fstream>
#include <iostream>

PpmImage::PpmImage()
{
}

PpmImage::PpmImage( std::string filename, int width, int height )
{
  Create( filename, width, height );
}

PpmImage::PpmImage( std::string filename )
{
  Open( filename );
}

void PpmImage::Create( std::string filename, int width, int height )
{
  m_filename = filename;
  CreateCanvas( width, height );
}

void PpmImage::Open( std::string filename )
{
  Log::Out( "Open " + filename, "PpmImage::Open" );

  if ( filename == "" ) { filename = m_filename; }
  std::ifstream input( filename );
  if ( input.fail() )
    {
      Log::Err( "Error opening file \"" + filename + "\"", "PpmImage::Open" );
      return;
    }
  std::string buffer;
  getline( input, buffer ); // P3
  getline( input, buffer ); // Comment
  input >> m_width >> m_height;
  input >> buffer; // Color depth

  Log::Out( "Read in pixels", "PpmImage::Open" );
  Pixel newPixel;
  int x = 0, y = 0;
  while ( input >> newPixel.r >> newPixel.g >> newPixel.b )
    {
      // Figure out ncurses color
      newPixel.backgroundColor = RgbToColorName( newPixel.r, newPixel.g, newPixel.b );

      // Set up coordinates
      newPixel.x = x;
      newPixel.y = y;
      x++;
      if ( x >= m_width )
      {
        x = 0;
        y++;
      }

      m_pixels.push_back( newPixel );
    }
}

void PpmImage::Save( std::string filename )
{
  if ( filename == "" ) { filename = m_filename; }
  std::ofstream output( filename );

  output << "P3" << std::endl;
  output << "# Created with Cursed Paint" << std::endl;
  output << m_width << " " << m_height << std::endl;
  output << "255" << std::endl;
  for ( auto& px : m_pixels )
    {
      output << px.r << std::endl << px.g << std::endl << px.b << std::endl;
    }
}

int PpmImage::GetWidth() const
{
  return m_width;
}

int PpmImage::GetHeight() const
{
  return m_height;
}

void PpmImage::SetDimensions( int width, int height )
{
  m_width = width;
  m_height = height;
}

std::vector<Pixel>& PpmImage::GetPixels()
{
  return m_pixels;
}

void PpmImage::CreateCanvas( int width, int height )
{
  SetDimensions( width, height );
  for ( int y = 0; y < height; y++ )
    {
      for ( int x = 0; x < width; x++ )
        {
          Pixel newPixel;
	  newPixel.SetXy( x, y );
	  if ( ( x + y ) % 2 == 0 )
	    {
	      newPixel.SetRgb( 255, 0, 255 );
	      newPixel.backgroundColor = "magenta";
	    }
	  else
	    {
	      newPixel.SetRgb( 0, 255, 255 );
	      newPixel.backgroundColor = "cyan";
	    }
	  m_pixels.push_back( newPixel );
        }
    }
}

std::string PpmImage::RgbToColorName( int r, int g, int b )
{
  if      ( r == 0 && g == 0 && b == 0 ) { return "black"; }
  else if ( r == 0 && g == 0 && b == 255 ) { return "blue"; }
  else if ( r == 0 && g == 255 && b == 0 ) { return "green"; }
  else if ( r == 0 && g == 255 && b == 255 ) { return "cyan"; }
  else if ( r == 255 && g == 0 && b == 0 ) { return "red"; }
  else if ( r == 255 && g == 0 && b == 255 ) { return "magenta"; }
  else if ( r == 255 && g == 255 && b == 0 ) { return "yellow"; }
  else if ( r == 255 && g == 255 && b == 255 ) { return "white"; }
  else { return "CLEAR"; } // default
}

RGB PpmImage::ColorNameToRgb( std::string color )
{
  if ( color == "black" ) { return RGB( 0, 0, 0 ); }
  else if ( color == "blue" ) { return RGB( 0, 0, 255 ); }
  else if ( color == "green" ) { return RGB( 0, 255, 0 ); }
  else if ( color == "cyan" ) { return RGB( 0, 255, 255 ); }
  else if ( color == "red" ) { return RGB( 255, 0, 0 ); }
  else if ( color == "magenta" ) { return RGB( 255, 0, 255 ); }
  else if ( color == "yellow" ) { return RGB( 255, 255, 0 ); }
  else if ( color == "white" ) { return RGB( 255, 255, 255 ); }
  else { return RGB( 6, 6, 6 ); }
}

std::string PpmImage::GetGoodTextColor( std::string backgroundColor )
{
  if ( backgroundColor == "black" || backgroundColor == "blue" || backgroundColor == "red" )
    {
      return "white";
    }
  else
    {
      return "black";
    }
}

void PpmImage::SetPixelColor( int x, int y, std::string color )
{
  for ( auto& px : m_pixels )
    {
      if ( px.x == x && px.y == y )
	{
	  px.backgroundColor = color;
	  RGB rgb = ColorNameToRgb( color );
	  px.r = rgb.r;
	  px.g = rgb.g;
	  px.b = rgb.b;
	  return;
	}
    }
}

void PpmImage::FillAllPixels( std::string color )
{
  RGB rgb = ColorNameToRgb( color );
  for ( auto& px : m_pixels )
    {
      px.backgroundColor = color;
      px.r = rgb.r;
      px.g = rgb.g;
      px.b = rgb.b;
    }
}

std::string PpmImage::GetFilename() const
{
  return m_filename;
}
