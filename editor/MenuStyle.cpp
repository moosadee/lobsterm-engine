#include "MenuStyle.h"

#include <iostream>
using namespace std;

void Header( string text, int level /* =1 */ )
{
    text = "- " + text + " -";
    cout << string( 80, '-' ) << endl;
    cout << text << endl;
    cout << string( text.size(), '-' ) << endl;
    cout << endl;
}

string Menu( vector<string> options )
{
    for ( size_t i = 0; i < options.size(); i++ )
    {
	cout << i << ". " << options[i] << endl;
	if ( i == 0 ) { cout << endl; }
    }
    cout << endl;
    size_t result = GetInput( 0, options.size()-1 );
    return options[ result ];
}

int GetInput( int min, int max )
{
    int choice;
    cout << "(" << min << "-" << max << ") >> ";
    cin >> choice;
    while ( choice < min || choice > max )
    {
        cout << "Invalid selection, try again." << endl;
        cout << "(" << min << "-" << max << ") >> ";
        cin >> choice;
    }

    return choice;
}
