#ifndef _PROGRAM
#define _PROGRAM

#include <vector>
#include <string>
using namespace std;

class Program
{
public:
    Program();
    ~Program();

    void Setup();
    void Teardown();
    void Run();
    
    void Menu_Main();
    void Menu_NewProject();
    
private:
    string m_projectName;
};

#endif
