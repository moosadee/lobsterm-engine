#ifndef _MENUSTYLE
#define _MENUSTYLE

#include <vector>
#include <string>
using namespace std;

void Header( string text, int level = 1 );
string Menu( vector<string> options );
int GetInput( int min, int max );

#endif
