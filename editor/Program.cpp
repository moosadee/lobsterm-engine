#include "Program.h"

#include <iostream>
#include <iomanip>
using namespace std;

#include "MenuStyle.h"
#include "Utilities.h"

Program::Program() { Setup(); }
Program::~Program() { Teardown(); }
void Program::Run() { Menu_Main(); }

void Program::Setup()
{
}

void Program::Teardown()
{
}

void Program::Menu_Main()
{
    bool menuRunning = true;
    while ( menuRunning )
    {
	Header( "MAIN MENU" );
	string selection = Menu( {
		"QUIT",
		"NEW PROJECT",
		"LOAD PROJECT"
	    } );

	if ( selection == "QUIT" )
	{
	    menuRunning = false;
	}
	else if ( selection == "NEW PROJECT" )
	{
	    Menu_NewProject();
	}
	else if ( selection == "LOAD PROJECT" )
	{
	}
    }
}

void Program::Menu_NewProject()
{
    Header( "NEW PROJECT" );
    string projectTitle;
    cout << "Name of the project: ";
    cin.ignore();
    getline( cin, projectTitle );

    string cleanTitle = Replace( projectTitle, " ", "_" );
	
    string command = "mkdir " + cleanTitle;
    system( command.c_str() );

    cout << "Created folder \"" << cleanTitle << "\"" << endl;
}

int main( int argCount, char* args[] )
{
    Program program;
    program.Run();
    
    return 0;
}
