#include "Utilities.h"

string Replace( string fulltext, string findme, string replacewith )
{
    
    string updated = fulltext;
    size_t index = updated.find( findme );

    while ( index != std::string::npos )
    {
        // Make the replacement
        updated = updated.replace( index, findme.size(), replacewith );

        // Look for the item again
        index = updated.find( findme );
    }

    return updated;
}
